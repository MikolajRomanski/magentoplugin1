<?php

$installer = $this;
$installer->startSetup();

$setup = null;

$attributes = Mage::getModel('customer/customer') -> getAttributes();

$attr = 0;
if(empty($attributes['mobile'])) {
	if($setup == null) $setup = new Mage_Eav_Model_Entity_Setup('core_setup');
	$entityTypeId     = $setup->getEntityTypeId('customer');
	$attributeSetId   = $setup->getDefaultAttributeSetId($entityTypeId);
	$attributeGroupId = $setup->getDefaultAttributeGroupId($entityTypeId, $attributeSetId);




	$setup->addAttribute('customer', 'mobile', array(
			'input'         => 'text',
			'type'          => 'varchar',
			'label'         => 'Mobile',
			'visible'       => 1,
			'required'      => 1,
			'user_defined' => 1,
	));

	$setup->addAttributeToGroup(
			$entityTypeId,
			$attributeSetId,
			$attributeGroupId,
			'mobile',
			'100'
	);

	$oAttribute = Mage::getSingleton('eav/config')->getAttribute('customer', 'mobile');
	$oAttribute->setData('used_in_forms', array('adminhtml_customer'));
	$oAttribute->save();
	$attr++;
}
if(empty($attributes['company'])) {
	$entityTypeId     = $setup->getEntityTypeId('customer');
	$attributeSetId   = $setup->getDefaultAttributeSetId($entityTypeId);
	$attributeGroupId = $setup->getDefaultAttributeGroupId($entityTypeId, $attributeSetId);




	$setup->addAttribute('customer', 'company', array(
			'input'         => 'text',
			'type'          => 'varchar',
			'label'         => 'Company',
			'visible'       => 1,
			'required'      => 1,
			'user_defined' => 1,
	));

	$setup->addAttributeToGroup(
			$entityTypeId,
			$attributeSetId,
			$attributeGroupId,
			'company',
			'100'
	);

	$oAttribute = Mage::getSingleton('eav/config')->getAttribute('customer', 'company');
	$oAttribute->setData('used_in_forms', array('adminhtml_customer'));
	$oAttribute->save();
	$attr++;
}
if(empty($attributes['vat_number'])) {

	$entityTypeId     = $setup->getEntityTypeId('customer');
	$attributeSetId   = $setup->getDefaultAttributeSetId($entityTypeId);
	$attributeGroupId = $setup->getDefaultAttributeGroupId($entityTypeId, $attributeSetId);




	$setup->addAttribute('customer', 'vat_number', array(
			'input'         => 'text',
			'type'          => 'varchar',
			'label'         => 'Vat Number',
			'visible'       => 1,
			'required'      => 1,
			'user_defined' => 1,
	));

	$setup->addAttributeToGroup(
			$entityTypeId,
			$attributeSetId,
			$attributeGroupId,
			'vat_number',
			'100'
	);

	$oAttribute = Mage::getSingleton('eav/config')->getAttribute('customer', 'vat_number');
	$oAttribute->setData('used_in_forms', array('adminhtml_customer'));
	$oAttribute->save();

	$attr++;
}

$setup->endSetup();