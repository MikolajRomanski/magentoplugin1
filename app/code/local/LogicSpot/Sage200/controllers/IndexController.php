<?php
class LogicSpot_Sage200_IndexController extends Mage_Core_Controller_Front_Action {
	private $storepath = null;
	private $outgoingcustomerpath = 'outgoing/customers/';
	private $outgoingorderspath = 'outgoing/orders/';
	private $incomingcustomerpath = 'incoming/customers/';
	private $incominggrouppath = 'incoming/customers/';
	private $incomingorderspath = 'incoming/orders/';
	private $incomingproductspath = 'incoming/products/';
	private $incomingstockpath = 'incoming/products/';
	private $incomingnewproductpath = 'incoming/products/';
	private $incomingcustomerprefix = 'Customer_Account_Export';
	private $incominggroupprefix = 'Customer_Discount_Group_Export';
	private $incomingproductpricesprefix = 'Stock_Item_Price_Export';
	private $incomingstockplevelprefix = 'Stock_Level_Export';
	private $incomingnewproductsprefix = 'Stock_Item_Export';
	private $customerimportgroup = null;
	private $customermovetogroup = null;
	private $customer_import_column_order = array (
			'AccountName',
			'Address_1',
			'Address_2',
			'Postcode',
			'City',
			'County',
			'Country',
			'First_name',
			'Last_name',
			'Telephone',
			'mobile',
			'Fax',
			'email',
			'Vat_number' 
	);
	private $group_import_column_order = array (
			'CustomerName',
			'DiscountGroup' 
	);
	private $product_prices_import_column_order = array (
			'StockItem',
			'Price' 
	);
	private $product_stocklevel_import_column_order = array (
			'StockItem',
			'StockLevel' 
	);
	private $new_product_import_column_order = array (
			'StockItemCode',
			'StockItemName',
			'ProductGroup' 
	);
	public function indexAction() {
		echo "<pre>";
		touch('/tmp/indexcontrolleraction'.date('Ymd-His'));
		$storeconfig = Mage::getStoreConfig ( 'sage200_options/messages/storepathexport' );
		echo "\n\nSTOREPATH: $storeconfig\n\n";
		if (! empty ( $storeconfig )) {
			if (is_dir ( getcwd () . '/' . $storeconfig )) {
				$this->storepath = getcwd () . '/' . $storeconfig;
				// be sure it ends with '/'
				$this->storepath = rtrim ( $this->storepath, '/' ) . '/';
			} else {
				Mage::log ( "Configured export path '" . $this->storepath . "' doesn't exists!", null, 'polcode.log', true );
				return false;
			}
		}
		
		if (! is_writable ( $this->storepath )) {
			Mage::log ( "Configured export path '" . $this->storepath . "' is not writeable!", null, 'polcode.log', true );
			// throw new Exception("Configured export path '".$this -> storepath."' is not writeable!");
		}
		
		// $this -> export_customers();
		// $this -> import_customers();
		// $this -> import_discount_groups();
		
		// $this -> import_stock_item_price();
		// $this -> import_stock_level();
		$this->import_new_products ();
	}
	
	/**
	 * Get customers from selected group
	 */
	private function export_customers() {
		$this->customerimportgroup = Mage::getStoreConfig ( 'sage200_options/sagecustomerexport/importgroup' );
		$this->customermovetogroup = Mage::getStoreConfig ( 'sage200_options/sagecustomerexport/movegroup' );
		
		$collection = Mage::getModel ( 'customer/customer' )->getCollection ()->addAttributeToSelect ( "*" )->addFieldToFilter ( 'group_id', $this->customerimportgroup );
		
		$export = $this->prepareCustomerExport ( $collection );
		
		if (! empty ( $export )) {
			
			$filename = $this->storepath . $this->outgoingpath . $this->outgoingcustomerpath . "customers-" . date ( 'YmdHis' ) . '.csv';
			$file = fopen ( $filename, "w" );
			$this->storeasCsv ( $file, array_keys ( $export [0] ) );
			
			foreach ( $export as $value ) {
				$userid = $value ['Id'];
				$this->storeasCsv ( $file, $value );
				$this->moveCustomerIntoGroup ( $userid, $this->customermovetogroup );
			}
			
			fclose ( $file );
		}
	}
	
	/**
	 * Prepare export rows in variable
	 * 
	 * @param unknown $collection        	
	 * @return array $export
	 */
	private function prepareCustomerExport($collection) {
		foreach ( $collection as $value ) {
			$bilingAddressId = $value->getDefaultBilling ();
			$bilingaddress = Mage::getModel ( 'customer/address' )->load ( $bilingAddressId );
			$shippingAddress = $value->getDefaultShippingAddress ();
			$export [] = array (
					'Id' => $value->getData ( 'entity_id' ),
					
					'Address_1' => $bilingaddress->getStreet ( 1 ),
					'Address_2' => $bilingaddress->getStreet ( 2 ),
					'Postcode' => $bilingaddress->getData ( 'postcode' ),
					'City' => $bilingaddress->getData ( 'city' ),
					'County' => $bilingaddress->getData ( 'region' ),
					'Telephone' => $bilingaddress->getData ( 'telephone' ),
					'Fax' => $bilingaddress->getData ( 'fax' ),
					'Country' => Mage::getModel ( "directory/country" )->load ( $bilingaddress->getData ( 'country_id' ) )->getName (),
					'company' => $value->getData ( 'company' ),
					'First_name' => $value->getData ( 'firstname' ),
					'Last_name' => $value->getData ( 'lastname' ),
					'mobile' => $value->getData ( 'mobile' ),
					'email' => $value->getData ( 'email' ),
					'Vat_number' => $value->getData ( 'Vat_number' ) 
			);
		}
		return $export;
	}
	
	/**
	 * Store single array row in file handler $fh in csv format, joined with ";"
	 * 
	 * @param filehandler $fh        	
	 * @param array $array        	
	 */
	private function storeasCsv($fh, $array) {
		$row = array ();
		foreach ( $array as $v ) {
			$row [] = addslashes ( $v );
		}
		// print_r($row);
		$string = implode ( '","', $row );
		
		fputs ( $fh, "\"$string\"" . "\n" );
	}
	
	/**
	 * Move customer $customer_id into group $group_id
	 * 
	 * @param int $customer_id        	
	 * @param int $group_id        	
	 */
	private function moveCustomerIntoGroup($customer_id, $group_id) {
		Mage::getModel ( 'customer/customer' )->load ( $customer_id )->setData ( 'group_id', $group_id )->save ();
	}
	
	/**
	 * Main method to import customers from file
	 * 
	 * @return boolean
	 */
	private function import_customers() {
		if (! is_dir ( $this->storepath . $this->incomingcustomerpath )) {
			return false;
		}
		
		$importfiles = $this->getFilesByName ( $this->incomingcustomerprefix, $this->storepath . $this->incomingcustomerpath );
		if (empty ( $importfiles ))
			return false;
		foreach ( $importfiles as $value ) {
			$this->import_customer_from_csv_file ( $this->storepath . $this->incomingcustomerpath . $value );
		}
	}
	private function getFilesByName($prefix, $path) {
		$filelist = array ();
		// echo "\nsearch prefix: $prefix / $path\n";
		$dir = opendir ( $path );
		while ( $file = readdir ( $dir ) ) {
			// echo "\n\ntest $file\n";
			
			if (preg_match ( "/^$prefix/", $file )) {
				// echo "underfile!! \n";
				$filelist [] = $file;
			}
		}
		
		asort ( $filelist );
		
		// echo "\nAvaiable files:\n";
		// print_r($filelist);
		// echo "End of avaiable files!\n\n";
		return ($filelist);
	}
	private function import_customer_from_csv_file($filepath) {
		$file = file ( $filepath );
		$csv = str_getcsv ( $file [0], ',', '"', '\\' );
		$format = $this->check_cusomer_import_file ( $csv );
		if (! $format) {
			Mage::log ( "Import file " . $filepath . " bad format", null, 'polcode.log', true );
			return false;
		}
		
		foreach ( $file as $key => $value ) {
			$value = trim ( $value );
			$row = $this->customer_import_row ( str_getcsv ( $value, ',', '"', '\\' ) );
			if ($row == '')
				continue;
			$customer_id = $this->getCustomerIdByCompany ( $row ['AccountName'] );
			// echo "customerid: $customer_id\n";
			if (empty ( $customer_id ))
				continue;
				// echo "not null \n";
			$this->updateCustomer ( $customer_id, $row );
		}
	}
	
	/**
	 * Check file format
	 * 
	 * @deprecated - please use method: check_import_file($file, $matcharray)
	 * @param unknown $file        	
	 * @return boolean
	 */
	private function check_cusomer_import_file($file) {
		return $this->check_import_file ( $file, $this->customer_import_column_order );
	}
	
	/**
	 *
	 * @deprecated Use reate_import_row($value, $targetArray)
	 * @param unknown $value        	
	 * @return Ambigous <string, multitype:string >|string|multitype:string
	 */
	private function customer_import_row($value) {
		return $this->create_import_row ( $value, $this->customer_import_column_order, 'customer_import' );
		
		static $rowconfig = null;
		// first row as column names
		if ($rowconfig == null) {
			$temparray = array ();
			$rowconfig = array ();
			foreach ( $this->customer_import_column_order as $key => $v ) {
				$temparray [$v] = $key;
			}
			foreach ( $value as $key => $v ) {
				$rowconfig [$key] = $v;
			}
			return '';
		}
		
		$array = array ();
		foreach ( $value as $key => $v ) {
			$array [$rowconfig [$key]] = trim ( stripslashes ( $v ) );
		}
		
		return $array;
	}
	
	/**
	 * String into
	 * 
	 * @param unknown $value        	
	 * @param unknown $targetArray        	
	 * @param unknown $fixname        	
	 * @return string multitype:string
	 */
	private function create_import_row($value, $targetArray, $fixname) {
		static $rowconfig = array ();
		
		// first row as column names
		if ($rowconfig [$fixname] == null) {
			$temparray = array ();
			$rowconfig [$fixname] = array ();
			
			foreach ( $targetArray as $key => $v ) {
				$temparray [$v] = $key;
			}
			
			foreach ( $value as $key => $v ) {
				$rowconfig [$fixname] [$key] = $v;
			}
			
			return '';
		}
		
		$array = array ();
		foreach ( $value as $key => $v ) {
			$array [$rowconfig [$fixname] [$key]] = trim ( stripslashes ( $v ) );
		}
		
		return $array;
	}
	private function getCustomerIdByCompany($companyname) {
		$customer = Mage::getModel ( 'customer/customer' )->getCollection ()->addFieldToFilter ( 'company', $companyname );
		$i = 0;
		foreach ( $customer as $value ) {
			$i ++;
			$data = $value->getData ( 'entity_id' );
			if ($i > 0)
				break;
		}
		if (! empty ( $data )) {
			return $data;
		}
		return null;
	}
	
	/**
	 * Update customer informations
	 * 
	 * @param unknown $customer_id        	
	 * @param unknown $array        	
	 */
	private function updateCustomer($customer_id, $array) {
		// print_r($array);
		$customer = Mage::getModel ( 'customer/customer' )->load ( $customer_id );
		$bilingAddressId = $customer->getDefaultBilling ();
		$bilingaddress = Mage::getModel ( 'customer/address' )->load ( $bilingAddressId );
		$bilingaddress->setStreet ( array (
				$array ['Address_1'],
				$array ['Address_2'] 
		) );
		$bilingaddress->setData ( 'postcode', $array ['Postcode'] . 'editpost' );
		$bilingaddress->setData ( 'city', $array ['City'] );
		$bilingaddress->setData ( 'region', $array ['County'] );
		$countryid = $this->getCountryIdByName ( $array ['Country'] );
		if ($countryid !== false)
			$bilingaddress->setCountryId ( $countryid );
		
		$customer->setData ( 'firstname', $array ['First_name'] );
		$customer->setData ( 'lastname', $array ['Last_name'] );
		$customer->setData ( 'vat_number', $array ['Vat_number'] );
		$customer->setData ( 'telephone', $array ['Telephone'] );
		$customer->setData ( 'fax', $array ['Fax'] );
		$customer->setData ( 'mobile', $array ['mobile'] );
		$customer->setData ( 'email', $array ['email'] );
		
		$bilingaddress->save ();
		$customer->save ();
	}
	private function getCountryIdByName($name) {
		static $clist = array ();
		if (empty ( $clist )) {
			$clist_ = Mage::getModel ( 'directory/country_api' )->items ();
			foreach ( $clist_ as $value ) {
				$clist [$value ['name']] = $value ['country_id'];
			}
		}
		
		return empty ( $clist [$name] ) ? false : $clist [$name];
	}
	private function import_discount_groups() {
		$importfiles = $this->getFilesByName ( $this->incominggroupprefix, $this->storepath . $this->incominggrouppath );
		
		if (empty ( $importfiles )) {
			// echo "no group files";
			return false;
		}
		
		foreach ( $importfiles as $value ) {
			$this->import_group_from_csv_file ( $this->storepath . $this->incominggrouppath . $value );
		}
		return true;
	}
	
	/**
	 * Import groups from csv file
	 * 
	 * @param string $filepath
	 *        	file path for import
	 * @return boolean
	 */
	private function import_group_from_csv_file($filepath) {
		$file = file ( $filepath );
		$csv = str_getcsv ( $file [0], ',', '"', '\\' );
		$format = $this->check_import_file ( $csv, $this->group_import_column_order );
		
		if (! $format) {
			Mage::log ( "Import file " . $filepath . " bad format", null, 'polcode.log', true );
			return false;
		}
		// print_r($file);
		foreach ( $file as $value ) {
			// echo $value."\n";
			$value = trim ( $value );
			$row = $this->create_import_row ( str_getcsv ( $value, ',', '"', '\\' ), $this->group_import_column_order, 'discount_groups' );
			
			if ($row == '')
				continue;
			$customer_id = $this->getCustomerIdByCompany ( $row ['CustomerName'] );
			$group_id = $this->getCustomerGroupIdByName ( $row ['DiscountGroup'] );
			
			if (empty ( $group_id )) {
				Mage::log ( "Import discount groups - group name '$row[DiscountGroup]' doesn't exists", null, 'polcode.log', true );
				return false;
			}
			
			// echo "$customer_id#$group_id\n";
			Mage::getModel ( 'customer/customer' )->load ( $customer_id )->setData ( 'group_id', $group_id )->save ();
		}
		return true;
	}
	
	/**
	 * Check if all columns are at import file.
	 * It will ignore to check if there are some uncecessary columns.
	 * It also assign variables into its headers name
	 * 
	 * @param array $file
	 *        	- header row (first row of import file)
	 * @param array $array
	 *        	- inventory due columns
	 * @return boolean
	 */
	private function check_import_file($file, $array) {
		$sum = count ( $array );
		
		foreach ( $file as $value ) {
			if (in_array ( $value, $array ))
				$sum --;
		}
		
		if ($sum != 0) {
			return false;
		}
		
		return true;
	}
	private function getCustomerGroupIdByName($groupname) {
		$customer_group = new Mage_Customer_Model_Group ();
		$allGroups = $customer_group->getCollection ()->toOptionHash ();
		foreach ( $allGroups as $key => $value ) {
			if ($groupname == $value)
				return $key;
		}
		
		return '';
	}
	
	/**
	 * Main method for import stock product prices by SKU
	 * 
	 * @return boolean
	 */
	private function import_stock_item_price() {
		$importfiles = $this->getFilesByName ( $this->incomingproductpricesprefix, $this->storepath . $this->incomingproductspath );
		if (empty ( $importfiles )) {
			// echo "no group files";
			return false;
		}
		
		foreach ( $importfiles as $value ) {
			$this->import_product_prices_from_csv_file ( $this->storepath . $this->incomingproductspath . $value );
		}
		return true;
	}
	private function import_product_prices_from_csv_file($filepath) {
		$file = file ( $filepath );
		$csv = str_getcsv ( $file [0], ',', '"', '\\' );
		
		$format = $this->check_import_file ( $csv, $this->product_prices_import_column_order );
		
		if (! $format) {
			Mage::log ( "Import file " . $filepath . " bad format", null, 'polcode.log', true );
			return false;
		}
		
		foreach ( $file as $value ) {
			$value = trim ( $value );
			$row = $this->create_import_row ( str_getcsv ( $value, ',', '"', '\\' ), $this->product_prices_import_column_order, 'import_product_prices' );
			
			if ($row == '')
				continue;
			$product = $this->getProductBySKU ( $row ['StockItem'] );
			$price = $row ['Price'];
			
			if (empty ( $product )) {
				Mage::log ( "Import stock product price error: SKU '$row[StockItem]' doesn't exists", null, 'polcode.log', true );
				continue;
			}
			
			$product->setData ( 'price', $price );
			$product->save ();
		}
		return true;
	}
	private function getProductBySKU($sku) {
		$product = Mage::getModel ( 'catalog/product' )->getCollection ()->addAttributeToSelect ( '*' )->addAttributeToFilter ( 'sku', $sku );
		$productarray = array ();
		$productids = array ();
		foreach ( $product as $value ) {
			$productarray [] = $value;
			$productids [] = $value->getData ( 'entity_id' );
		}
		
		if (count ( $productsids ) > 1) {
			$ids = implode ( ", ", $productids );
			Mage::log ( 'Multiple products by single SKU: ' . $ids . ' Update ignored', null, 'polcode.log', true );
			return false;
		}
		
		return $productarray [0];
	}
	
	/**
	 * Main method for import stock levels by SKU
	 * 
	 * @return boolean
	 */
	private function import_stock_level() {
		$importfiles = $this->getFilesByName ( $this->incomingstockplevelprefix, $this->storepath . $this->incomingstockpath );
		if (empty ( $importfiles )) {
			return false;
		}
		
		foreach ( $importfiles as $value ) {
			$this->import_stocks_from_csv_file ( $this->storepath . $this->incomingstockpath . $value );
		}
		
		return true;
	}
	private function import_stocks_from_csv_file($filepath) {
		$file = file ( $filepath );
		$csv = str_getcsv ( $file [0], ',', '"', '\\' );
		
		$format = $this->check_import_file ( $csv, $this->product_stocklevel_import_column_order );
		
		if (! $format) {
			Mage::log ( "Import file " . $filepath . " bad format", null, 'polcode.log', true );
			return false;
		}
		
		foreach ( $file as $value ) {
			$value = trim ( $value );
			$row = $this->create_import_row ( str_getcsv ( $value, ',', '"', '\\' ), $this->product_stocklevel_import_column_order, 'import_product_stock' );
			
			if ($row == '')
				continue;
			$product = $this->getProductBySKU ( $row ['StockItem'] );
			$price = $row ['Price'];
			
			if (empty ( $product )) {
				Mage::log ( "Import stock product amount error: SKU '$row[StockItem]' doesn't exists", null, 'polcode.log', true );
				continue;
			}
			
			$stock = Mage::getModel ( 'cataloginventory/stock_item' )->loadByProduct ( $product->getId () );
			$stock->setData ( 'qty', $row ['StockLevel'] );
			$stock->save ();
		}
		return true;
	}
	
	/**
	 * Main method for import new products
	 */
	private function import_new_products() {
		$importfiles = $this->getFilesByName ( $this->incomingnewproductsprefix, $this->storepath . $this->incomingnewproductpath );
		
		if (empty ( $importfiles )) {
			return false;
		}
		
		foreach ( $importfiles as $value ) {
			$this->import_new_products_from_csv_file ( $this->storepath . $this->incomingnewproductpath . $value );
		}
	}
	private function import_new_products_from_csv_file($filepath) {
		$file = file ( $filepath );
		$csv = str_getcsv ( $file [0], ',', '"', '\\' );
		
		$format = $this->check_import_file ( $csv, $this->new_product_import_column_order );
		
		if (! $format) {
			Mage::log ( "Import new product file " . $filepath . " bad format", null, 'polcode.log', true );
			return false;
		}
		
		foreach ( $file as $key => $value ) {
			echo "\nvalue: $value\n";
			
			$value = trim ( $value );
			
			$row = $this->create_import_row ( str_getcsv ( $value, ',', '"', '\\' ), $this->new_product_import_column_order, 'import_new_products' );
			
			if ($row == '')
				continue;
			
			if (preg_match ( "/^DO NOT USE/", $row ['StockItemCode'] )) {
				
				continue;
			} else {
				echo "stockitem: $row[StockItemCode]\n";
				echo "USE IT!\n";
				$this->create_new_product ( $row );
			}
			
			print_r ( $row );
		}
	}
	private function create_new_product($data) {
		$attribute_id = $this->getValidAttributeSetByName ( $data ['ProductGroup'] );
		
		if (empty ( $attribute_id )) {
			Mage::log ( "Attribute '$data[ProductGroup]' doesn't exists", null, 'polcode.log', true );
			echo "attr doesn't exists\n\n\n";
			return false;
		}
		
		$api = new Mage_Catalog_Model_Product_Api ();
		$productData = array ();
		$productData ['status'] = 2;
		$productData ['name'] = utf8_encode ( $data['StockItemName'] );
		print_r($productData);
		echo 'attrid: '.$attribute_id."\n";
		echo 'name: '.$data['StockItemCode']."\n";
		try{
		$new_product_id = $api->create ( 'simple', $attribute_id, $data['StockItemCode'], $productData);
		}
		catch(Exception $e){
			//print_r($e);
			
			Mage::log ( "Exeption at creating new product: '$data[StockItemCode]' ".$e -> getMessage(), null, 'polcode.log', true );
		}
		echo "new product id: $new_product_id \n\n\n";
		return true;
	}
	private function getValidAttributeSetByName($name) {
		static $grouparray = null;
		if ($grouparray == null) {
			// get Avaiable attributes set
			$attribute_api = new Mage_Catalog_Model_Product_Attribute_Set_Api ();
			$attribute_sets = $attribute_api->items ();
			foreach ( $attribute_sets as $value ) {
				$grouparray [$value ['name']] = $value ['set_id'];
			}
			
		}
		
		return empty ( $grouparray [$name] ) ? '' : $grouparray [$name];
	}
}